//This works
int redPin1 = 13;
int greenPin1 = 12;
int bluePin1 = 11;
int redPin2 = 10;
int greenPin2 = 9;
int bluePin2 = 8;
int redPin3 = 7;
int greenPin3 = 6;
int bluePin3 = 5;
int redPin4 = 4;
int greenPin4 = 3;
int bluePin4 = 2;

void setup() {
  Serial.begin(9600);
  pinMode(redPin1, OUTPUT);
  pinMode(greenPin1, OUTPUT);
  pinMode(bluePin1, OUTPUT);
  pinMode(redPin2, OUTPUT);
  pinMode(greenPin2, OUTPUT);
  pinMode(bluePin2, OUTPUT);
  pinMode(redPin3, OUTPUT);
  pinMode(greenPin3, OUTPUT);
  pinMode(bluePin3, OUTPUT);
  pinMode(redPin4, OUTPUT);
  pinMode(greenPin4, OUTPUT);
  pinMode(bluePin4, OUTPUT);
}



void loop() {
  //setColor(128, 0, 0);
  //delay(500);
  setColor(255, 0, 0);
  delay(500);
  //setColor(0, 128, 0);
  //delay(500);
  setColor(0, 255, 0);
  delay(500);
  //setColor(0, 0, 128);
  //delay(500);
  setColor(0, 0, 255);
  delay(500);
  
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin1, redValue);
  analogWrite(greenPin1, greenValue);
  analogWrite(bluePin1, blueValue);
  analogWrite(redPin2, redValue);
  analogWrite(greenPin2, greenValue);
  analogWrite(bluePin2, blueValue);
  analogWrite(redPin3, redValue);
  analogWrite(greenPin3, greenValue);
  analogWrite(bluePin3, blueValue);
  analogWrite(redPin4, redValue);
  analogWrite(greenPin4, greenValue);
  analogWrite(bluePin4, blueValue);
}
